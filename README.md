Terminal Forms (js)
==============

You give it a TTY, it gives you the best form-handling that it knows how!

```
var form = require('terminal-forms.js').create(process.stdin, process.stdout);
```

Input types presently supported:

* `form.ask(label, type)` where `type` is one of `form.inputs`
  * i.e. `form.ask("What is your quest? ", 'text').then(fn);`
* `form.setStatus(msg)`
  * i.e. `form.setStatus("(hint: you seek the Grail!)")`
* `form.inputs`
  * `text` (no constraints)
  * `email` (checks format and looks up MX records)
  * `url` (checks format and looks up A/AAAA/CNAME records)

Handlers
========

An input `type` handler may implement any or all of these interfaces:

* `onReturnAsync(rs, ws, input, ch)`
* `onDebounceAsync(rs, ws, input, ch)`

The follow options may also be specified:

* `debounceTimeout: ms`

```
{ onReturnAsync: function (rs, ws, input, ch) {
    // 1. pause the read stream if needed

    // 2. the write stream is given as a convenience for clearing the newline, etc

    // 3. check that input as a whole is valid

    // 4. check the most recent character, if desired

    // 5. normalize the input if desired (i.e. John.Doe@GMail.com -> john.doe@gmail.com)
    //    (or return something else entirely)

    // You can error out
    // return form.PromiseA.reject(new Error("[X] This isn't an email address: no '@'"));

    return input.toLowerCase(); // will be returned as `result` alongside `input`
  }


, onDebounceAsync: function (rs, ws, input, ch) {
    // Do a check on the input after 300ms without waiting for the return character

    // return true if the input is complete

    return false; // otherwise the input is not complete
  }
, debounceTimeout: 300 // default is 300
}
```

TODO

```
, onCharAsync: function (rs, ws, input, ch) {
    // the same as debounceTimeout 0
  }
```


Debugging
=========

### How to detect a pipe

Your run-of-the-mill bash scripts will not work if you require user input.

You can check to see if input or output is being handled by a pipe by checking the `isTTY` property.

* `process.stdin.isTTY`
* `process.stdout.isTTY`

```
node example.js
stdin.isTTY: true
stdout.isTTY: true
```

```
node bin/oauth3.js | grep ''
stdin.isTTY: true
stdout.isTTY: undefined
```

```
echo 'hello' | node bin/oauth3.js
stdin.isTTY: undefined
stdout.isTTY: true
```

```
echo 'hello' | node bin/oauth3.js | grep ''
stdin.isTTY: undefined
stdout.isTTY: undefined
```
